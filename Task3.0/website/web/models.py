from django.db import models
from time import time
import datetime

# Create your models here.
class Person(models.Model):
    clockedIn = models.BooleanField(default=False)
    firstName = models.CharField(max_length=250)
    lastName = models.CharField(max_length=250)
    STATUS = (
        ('1', ('Worker')),
        ('2', ('Sitemanager')),
    )
    status = models.CharField(
        max_length=32,
        choices=STATUS,
        default='none',
    )

    def punch(self, site):
        punch = Punch()
        if (self.clockedIn == True):
            punch.person = self
            punch.site = site
            punch.punchOut()
        else:
            punch.person = self
            punch.site = site
            punch.punchIn()


class Site(models.Model):
    site = models.CharField(max_length=250)


class Punch(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    site = models.ForeignKey(Site, on_delete=models.CASCADE)
    dateTime = models.DateTimeField()
    directionIn = models.BooleanField(default=False)

    def punchIn(self):
        self.dateTime = getTimeStamp()
        self.directionIn = True
        self.clockedIn = True
        self.save()

    def punchOut(self):
        self.dateTime = getTimeStamp()
        self.directionIn = False
        self.clockedIn = False
        self.save()


def getTimeStamp():
    ts = datetime.datetime.now()
    return ts





'''
class Site(models.Model):
    name = models.CharField(max_length=250)

class Person(models.Model):
    site = models.ManyToManyField(Site)
    clockedIn = models.BooleanField(default=False)
    firstName = models.CharField(max_length=250)
    lastName = models.CharField(max_length=250)
    STATUS = (
        ('1', ('Worker')),
        ('2', ('Sitemanager')),
    )
    status = models.CharField(
        max_length=32,
        choices=STATUS,
        default='none',
    )

    def punch(self, site):
        punch = Punch()
        if (self.clockedIn == True):
            punch.person = self
            punch.site = site
            punch.punchOut()
        else:
            punch.person = self
            punch.site = site
            punch.punchIn()




class Punch(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    dateTime = models.DateTimeField()
    directionIn = models.BooleanField(default=False)

    def punchIn(self):
        self.dateTime = getTimeStamp()
        self.directionIn = True
        self.clockedIn = True
        self.save()

    def punchOut(self):
        self.dateTime = getTimeStamp()
        self.directionIn = False
        self.clockedIn = False
        self.save()


def getTimeStamp():
    ts = datetime.datetime.now()
    return ts


class Site(models.Model):
    site = models.CharField(max_length=250)


class Person(models.Model):
    site = models.ManyToManyField(Site)
    firstName = models.CharField(max_length=250)
    lastName = models.CharField(max_length=250)
    STATUS = (
        ('1', ('Worker')),
        ('2', ('Sitemanager')),
    )
    status = models.CharField(
        max_length=32,
        choices=STATUS,
        default='none',
    )
    def punch(self):
        punch = Punch()
        if(self.clockedIn == True):
            punch.person = self
            punch.punchOut()
        else:
            punch.person = self
            punch.punchIn()



class Punch(models.Model):
    person = models.ForeignKey(Person,on_delete=models.CASCADE)
    dateTime = models.DateTimeField()
    directionIn = models.BooleanField(default=False)

    def punchIn(self):
        self.dateTime = getTimeStamp()
        self.directionIn = True
        self.clockedIn = True
        self.save()

    def punchOut(self):
        self.dateTime = getTimeStamp()
        self.directionIn = False
        self.clockedIn =False
        self.save()


def getTimeStamp():
    ts = datetime.datetime.now()
    return ts
from django.db import models
from time import time
import datetime

# Create your models here.
class Person(models.Model):
    firstName = models.CharField(max_length=250)
    lastName = models.CharField(max_length=250)
    STATUS = (
        ('1', ('Worker')),
        ('2', ('Sitemanager')),
    )
    status = models.CharField(
        max_length=32,
        choices=STATUS,
        default='none',
    )
    def site_for_punch(self):
        site = Site()
        site.person = self
        site.punch()


class Site(models.Model):
    person = models.ForeignKey(Person,on_delete=models.CASCADE)
    site = models.CharField(max_length=250)
    clockedIn = models.BooleanField(default=False)

    def punch(self):
        punch = Punch()
        if(self.clockedIn == True):
            punch.person_site = self
            punch.punchOut()
        else:
            punch.person = self
            punch.punchIn()



class Punch(models.Model):
    person_site = models.ForeignKey(Site,on_delete=models.CASCADE)
    dateTime = models.DateTimeField()
    directionIn = models.BooleanField(default=False)

    def punchIn(self):
        self.dateTime = getTimeStamp()
        self.directionIn = True
        self.clockedIn = True
        self.save()

    def punchOut(self):
        self.dateTime = getTimeStamp()
        self.directionIn = False
        self.clockedIn =False
        self.save()


def getTimeStamp():
    ts = datetime.datetime.now()
    return ts



    def punch(self,site):
        punch = Punch()
        if(self.clockedIn == True):
            punch.person = self
            punch.punchOut()
        else:
            punch.person = self
            punch.punchIn()



        ###documentation = Documentation()
        ###documentation.punch = self
        ###self.create_documentation()
class Documentation(models.Model):
    punch = models.ForeignKey(Punch, on_delete=models.CASCADE)
    APPROVE_DECLINE = (
        ('1', ('Approve')),
        ('2', ('Decline')),
    )
    approve_decline = models.CharField(
        max_length=32,
        choices=APPROVE_DECLINE,
        default="NONE",
    )
    def create_documentation(self):
        if (self.approve_decline == "Approve"):
            documentation = self
            documentation.save()

        if(self.approve_decline == "Decline"):
            self.work = "geschraubt2" #add_work
            self.create_documentation()

    def add_work(self):
        pass






class Site(models.Model):
    name = models.CharField(max_length=250)

    def add_site(self):
        name = Site(self)
        name.save()


class Worker(models.Model):
    site = models.ManyToManyField(Site, on_delete=models.CASCADE)
    clockedIn = models.BooleanField(default=False)
    firstName = models.CharField(max_length=250)
    lastName = models.CharField(max_length=250)
    status = "Worker"

    def punch(self):
        punch = Punch()
        if(self.clockedIn == True):
            punch.person = self
            punch.punchOut()
        else:
            punch.person = self
            punch.punchIn()


class Sitemanager(models.Model):
    site = models.ManyToManyField(Site, on_delete=models.CASCADE)
    clockedIn = models.ForeignKey(Worker, on_delete=models.CASCADE)
    firstName = models.CharField(max_length=250)
    lastName = models.CharField(max_length=250)
    status = "Sitemanager"

    def punch_worker(worker):
        punch = Punch()
        if(worker.clockedIn == True):
            punch.person = worker
            punch.punchOut()
        else:
            punch.person = worker
            punch.punchIn()


class Punch(models.Model):
    person = models.ForeignKey(Worker, on_delete=models.CASCADE)
    dateTime = models.DateTimeField()
    directionIn = models.BooleanField(default=False)

    def punchIn(self):
        self.dateTime = getTimeStamp()
        self.directionIn = True
        self.clockedIn = True
        self.save()

    def punchOut(self):
        self.dateTime = getTimeStamp()
        self.directionIn = False
        self.clockedIn = False
        self.save()
        documentation = Documentation()
        documentation.add_work()


def getTimeStamp():
    ts = datetime.datetime.now()
    return ts

class Documentation(models.Model):
    work = models.CharField(max_length=250)

    def add_work(self):
        work = "hallo"
        self.create_documentation()


    def create_documentation(self):

        self.save()
        pass




class PunchOut(models.Model):
    timeOut = models.TimeField()

    def punchIn(self):
        self.timeOut = time.time()
        return self.timeOut

class Punch(models.Model):
    site = models.ForeignKey('Site', on_delete=models.CASCADE)
    worker = models.ForeignKey('Worker', on_delete=models.CASCADE)
    timeCard = {}
    clockedIn = False


    def clockInOut(self):
            if self.clockedIn == False:
                self.timeCard["in"] = getTimeStamp()
                self.clockedIn = True
            else:
                self.timeCard["out"] = getTimeStamp()
                self.clockedIn = False
                self.documentation = Documentation()

def getTimeStamp():
    ts = time.time()
    return ts




class Course(models.Model):
        price = models.FloatField("Price", blank=True, null=True)
        voucher_id = models.CharField(max_length=255,blank=True, null=True)
        voucher_amount = models.IntegerField(blank=True, null=True)

        def __str__(self):
            return self.course_name

        @property
        def discounted_amount(self):
             return (self.voucher_amount/100)*self.price


course = Course.objects.get(id=1)
course.discounted_amount








class User (models.Model):

#Person
class Person(models.Model):
    firstName = models.CharField(max_length=250)
    lastName = models.CharField(max_length=250)

#Site
class Site(models.Model):
    site = models.CharField(max_length=250)

#Sitemanager
class Sitemanager (Person):
    site = models.OneToOneField('Site',on_delete=models.CASCADE,primary_key=True)


    def addSitemanager(self):
        self.firstname = Person.firstName
        self.lastName = Person.lastName
        self.site = Site.site
        self.satus = "SA"
        #alles speichern mit User


        def AproveDeclineEdit(self):  #ausgabe für doc und 2 Buttons für approve decline
            doc = Documentation.documentation
            saveDeclineEdit = input("What would you like to do?(Possible anwers: Aprove, Decline)")
            if saveDeclineEdit == "Aprove":
                Documentation.documentation.status = "Approved"
            elif saveDeclineEdit == "Decline":
                Documentation.documentation.status = "Declined"


        def punchmanual(self, worker, direction):
            if direction == 'in':
                punch = Punch.objects.create(site=self.site, worker=self.worker)
                punch.clockInOut()
            elif direction == 'out':
                punch = Punch.objects.get(site=self.site, worker=self.worker)
                punch.clockInOut()


#Worker
class Worker(Person):
    site = models.OneToOneField('Site',on_delete=models.CASCADE,primary_key=True)


    def addWorker(self):
        self.firstname = Person.firstName
        self.lastName = Person.lastName
        self.site = Site.site
        self.satus = "W"
        #alles speichern mit User

    def addWorkForDocumentation(self, documentation):
        doc = Documentation.documentation #ausgabe von Documentation
        self.work = models.CharField(max_length=250)
        documentation.status = "Waiting"

    def declinedAddWork(self, documentation):
        if documentation.status == "declined":
            print(documentation)#ändern?
            self.work = models.CharField(max_length=250)
            documentation.status = "Waiting"
        else:
            continue     #was ist das problem hier


#Documentation
class Documentation(models.Model):
    worker = models.ForeignKey('Worker', related_name="documentations")
    site = models.ForeignKey('Site', related_name="documentations")
    timecard = {}
    status = 'Missing Data'
    worker_documentation = []

    def documentation(self, punch):
        self.worker = punch.worker
        self.site = punch.site
        self.timecard = punch.timecard


#Punch
class Punch(Person,Site):
    site = models.ForeignKey('Site', on_delete=models.CASCADE)
    worker = models.ForeignKey('Worker', on_delete=models.CASCADE)
    timeCard = {}
    clockedIn = False


    def clockInOut(self, forgot = True):
        if forgot == True:
            if self.clockedIn == False:
                self.timeCard["in"] = getTimeStamp()
                self.clockedIn = True
            else:
                self.timeCard["out"] = getTimeStamp()
                self.clockedIn = False
                self.documentation = Documentation()


def getTimeStamp():
    ts = time.time()
    return ts
'''


